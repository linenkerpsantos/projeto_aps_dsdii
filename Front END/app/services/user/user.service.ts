import { User } from './../../model/user';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { PROJ_AUX_MOB } from '../auxmob.api';

@Injectable()
export class UserService {
  
  constructor(private http: HttpClient) {}

  login(user: User){
    return this.http.post(`${PROJ_AUX_MOB}/api/auth`,user);
  }

  createOrUpdate(user: User){
    if(user.id != null && user.id != ''){
      return this.http.put(`${PROJ_AUX_MOB}/api/user`,user);
    } else {
      user.id = null;
      return this.http.post(`${PROJ_AUX_MOB}/api/user`, user);
    }
  }

  findAll(page:number,count:number){
    return this.http.get(`${PROJ_AUX_MOB}/api/user/${page}/${count}`);
  }

  findById(id:string){
    return this.http.get(`${PROJ_AUX_MOB}/api/user/${id}`);
  }

  delete(id:string){
    return this.http.delete(`${PROJ_AUX_MOB}/api/user/${id}`);
  }
}
