package projetoaps.auxiliomobilidade.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import projetoaps.auxiliomobilidade.api.entity.ChangeStatus;

public interface ChangeStatusRepository extends MongoRepository<ChangeStatus, String> {

	Iterable<ChangeStatus> findByTicketIdOrderByDateChangeStatusDesc(String ticketId);
}
