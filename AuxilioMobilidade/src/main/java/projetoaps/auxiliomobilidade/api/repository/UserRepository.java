package projetoaps.auxiliomobilidade.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import projetoaps.auxiliomobilidade.api.entity.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmail(String email);

}