package projetoaps.auxiliomobilidade.api.dto;

import java.io.Serializable;

public class Summary implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer amountNovo;
	private Integer amountResolvido;
	private Integer amountAprovado;
	private Integer amountDesaprovado;
	private Integer amountAtribuido;
	private Integer amountFechado;

	public Integer getAmountNew() {
		return amountNovo;
	}

	public void setAmountNew(Integer amountNovo) {
		this.amountNovo = amountNovo;
	}

	public Integer getAmountResolved() {
		return amountResolvido;
	}

	public void setAmountResolved(Integer amountResolvido) {
		this.amountResolvido = amountResolvido;
	}

	public Integer getAmountDisapproved() {
		return amountDesaprovado;
	}

	public void setAmountDisapproved(Integer amountDesaprovado) {
		this.amountDesaprovado = amountDesaprovado;
	}

	public Integer getAmountApproved() {
		return amountAprovado;
	}

	public void setAmountApproved(Integer amountAprovado) {
		this.amountAprovado = amountAprovado;
	}

	public Integer getAmountAssigned() {
		return amountAtribuido;
	}

	public void setAmountAssigned(Integer amountAtribuido) {
		this.amountAtribuido = amountAtribuido;
	}

	public Integer getAmountClosed() {
		return amountFechado;
	}

	public void setAmountClosed(Integer amountFechado) {
		this.amountFechado = amountFechado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}