package projetoaps.auxiliomobilidade.api.enums;

public enum StatusEnum {

	Novo,
	Atribuido,
	Resolvido,
	Aprovado,
	Desaprovado,
	Fechar;
	
	public static StatusEnum getStatus(String status) {
		switch (status) {
		case "Novo" : return Novo;
		case "Resolvido" : return Resolvido;
		case "Aprovado" : return Aprovado;
		case "Desaprovado" : return Desaprovado;
		case "Atribuido" : return Atribuido;
		case "Fechar" : return Fechar;
		default : return Novo;
		}
	}

}
