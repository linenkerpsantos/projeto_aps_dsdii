package projetoaps.auxiliomobilidade.api.enums;
public enum ProfileEnum {
	
	ROLE_ADMIN,
	ROLE_ATENDENTE,
	ROLE_AREA;
}
