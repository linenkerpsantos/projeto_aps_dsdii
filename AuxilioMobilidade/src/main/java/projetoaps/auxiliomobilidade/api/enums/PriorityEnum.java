package projetoaps.auxiliomobilidade.api.enums;

public enum PriorityEnum {
	
	AGENDAMENTO,
	EMERGENCIA,
	RECLAMACAO;	

}
